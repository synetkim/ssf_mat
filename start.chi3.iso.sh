#!/bin/bash

db=/data-local/sykim/database/isolated
ctl=/data-local/sykim/database/isolated.flist

function run() {
echo "Start computing SSF 2: $1 "
out=$1
mkdir -p $out
runscriptm.sh compute $ctl $out
sleep 1;
}

run out.sf2.isolated/
