function [ret] = compute(ctl_file_path, outpath)
disp(ctl_file_path);
[fid,message]=fopen(ctl_file_path);
message;
filnam=textscan(fid,'%s');
fclose(fid)

for i=1:length(filnam{1})
        fprintf('%d / %d =============================', i, length(filnam{1}));
        infile = filnam{1}{i}
        C=strsplit(filnam{1}{i}, '/');
        tmp = strcat(outpath, C(end))
        outfile = char(strrep(tmp,'.wav','.sf2'));

        if exist(outfile)==2
                disp('skip');
        else
                disp('compute');
                SSF_INTERSPEECH2010(outfile, infile, 2, 0.075, 0.4);
        end
end
fprintf('\n\n===========================================\n');
fprintf('compute finish: out:%s\n', path);
fprintf('===========================================\n\n');
end
