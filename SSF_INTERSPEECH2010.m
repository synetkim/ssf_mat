%
% Programmed by Chanwoo Kim
%
% SSF Algorithm
% 
% Parameters
%
% szOutFileName : Output file in wave file format
% szInFileName  : Input file in wave file format
% iSSFType      : Either 1 (SSF Type-I) or 2 (SSF Type-II)
% dFrameLen     : Use 0.05 or 0.075 (50 ms or 75 ms)
% dLambda       : Forgetting factor. 0.4 is a good selection

function [ad_y] = SSF_INTERSPEECH2010(szOutFileName, szInFileName, iSSFType, dFrameLen, dLambda)
    
    [ad_x, dSampRate] = wavread(szInFileName);
    if dSampRate ~= 16000
        disp('[WARNING] this algorithm has not been tested other than 16 kHz sampling rate case');
       ad_x = resample(ad_x, 16000, dSampRate);
    end
    
    dStdOrg         = std(ad_x);
    ad_y            = zeros(size(ad_x));
    
    iNumChan      = 40;
    dSampRate     = 16000;
    % c_0 coefficient
    % Used in equation (4) and (5) in the IS2010
    % paper
    d_c_0        = 1e-2;  
    
    iFL        = floor(dFrameLen * dSampRate);
	iFP        = floor(0.01      * dSampRate);
    iFFTSize   = 2 ^ceil(log(iFL) / log(2));
	iSpeechLen = length(ad_x);
   
    ad_X  = zeros(iFFTSize,  1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
	% Pre-emphasis
	%
    ad_x        = filter([1 -0.97], 1, ad_x);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
	% Gammatone-shape Frequency Response
	%
    aad_H = ComputeFilterResponse(iNumChan, iFFTSize);
    aad_H = abs(NormalizeFilterGain(aad_H));
    for i = 1 : iNumChan
        aad_symH(:, i) = abs([aad_H(:, i); flipud(aad_H(:, i))]);
    end

    adHalfSpec    = zeros(iFFTSize / 2, 1);
    ad_M          = zeros(iNumChan, 1);
    
    ad_wi          = zeros(iNumChan, 1);
    ad_P           = zeros(iNumChan, 1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Analysis
    %
    m = 0;
	for i = 0 : iFP : iSpeechLen - 1 - iFL 
		ad_x_st = ad_x(i + 1 : i + iFL) .* hamming(iFL);
		ad_X                   = fft(ad_x_st, iFFTSize);
        adHalfSpec             = abs(ad_X(1: iFFTSize / 2));
        ad_mu                  = zeros(1, iFFTSize / 2);
        ad_w                   = zeros(iNumChan, 1);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        % Calculating the Power
        %
        for l = 1 : iNumChan
            % Equation (2) in the paper
            % Due to symmetry of the spectrum using the lower half is fine
            ad_P(l) = (sum(((adHalfSpec .* aad_H(:, l)) .^ 2)));
        
            if m == 0
                ad_M(l) = ad_P(l);
            else
                % Equation (3) in the IS2010 paper
                ad_M(l)  = dLambda * ad_M(l) + (1 - dLambda) * ad_P(l);
            end
        
            % SSF-Type I case
            if iSSFType == 1
                   % Equation (4) in the IS2010 paper
                    ad_P1(l) = max(ad_P(l) - ad_M(l),  d_c_0 * ad_P(l));
                    % Equation (6) the IS2010 paper
                    d_w =  ad_P1(l) / max(ad_P(l), eps); 
                    
            % SSF-Type II case                    
            elseif iSSFType == 2
                    % Equation (5) in the IS2010 paper
                    ad_P2(l) = max(ad_P(l) - ad_M(l),  d_c_0 * ad_M(l));
                    % Equation (6) the IS2010 paper
                    d_w =  ad_P2(l) / max(ad_P(l), eps); 
                    
            % No processing case
            else
                     d_w = 1;
            end
            
             % Numerator part of Equation (7) in the IS2010 paper
            ad_mu = ad_mu + d_w * abs(aad_H(:, l))'; 
        end
        
        % Denominator part of Equation (7) in the IS2010 paper
        ad_mu      = ad_mu ./ sum(abs(aad_H')); 
        
        % Equation (8) in the IS2010 paper
        ad_mu_sym  = [ad_mu, fliplr(ad_mu)];  
        
        % Equation (9) in the IS2010 paper
        ad_y_st               = ifft(ad_X .* ad_mu_sym');
        
        % Overlap addition to resynthesize speech
        ad_y(i + 1 : i + iFL) = ad_y(i + 1 : i + iFL) + ad_y_st(1 : iFL);
        m = m + 1;
    end
    
    ad_y = real(ad_y);
    ad_y = filter(1, [1 -0.97], ad_y);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Scaling and writing the file
    %
    ad_y = (ad_y - mean(ad_y)) * dStdOrg / std(ad_y);
    dMaxAmp = max(abs(ad_y));
    
    if (dMaxAmp >= 1)
        ad_y = ad_y /dMaxAmp / 1.01; % To prevent clipping
    end

    wavwrite(ad_y, 16000, szOutFileName);
end    
